let {MONGO_DB} = require('../config');

class server{
    constructor(){
        this.mongoose = require('mongoose');
        this.mongoose.connect(MONGO_DB, {useNewUrlParser: true});
        this.ObjectId = this.mongoose.mongo.ObjectId;
        this.fileSchema = new this.mongoose.Schema({
            title: String,
            content: String,
            user_id: String,
            user_name: String,
            create_time: Date,
        });
        this.userSchema = new this.mongoose.Schema({
            name: String,
            password: String,
            create_time: Date,
            files: {}
        });
        this.folderSchema = new this.mongoose.Schema({
            title: String,
            user_id: String,
            user_name: String,
            create_time: Date,
            files: [[], []],   // [[folderIds], [fileIds]]
        })
    }

    result(success, error, message){
        return {
            success,
            error,
            message
        }
    };

    /**
     * ERROR TYPE: {
     *     'CLIENT_INPUT': '用户请求错误'
     * }
     *
     */
    inputError(info) {
        return {
            type: 'inputError',
            info
        }
    }

    databaseError(info) {
        return {
            type: 'databaseError',
            info
        }
    }

}

module.exports = server;


//** test
// se = new server();
// user = se.mongoose.model('user', se.userSchema, 'user');
// user.find({}, (err, doc) => {
//     console.log(doc);
// });
