let serve = require('./server');

class fileServer extends serve{
    constructor(props) {
        super(props);
        this.fileModule = this.mongoose.model('file', this.fileSchema, 'file');
    }

    update_file_content(_id, content){
        return new Promise((resolve, reject)=>{
            this.fileModule.updateOne({
                _id: this.ObjectId(_id)
            }, {
                content
            }, (err, res)=>{
                if (err) {
                    console.log(err);
                    reject(err);
                }
                resolve(res);
            });
        });
    }

    update_file_title(_id, title){
        return new Promise((resolve, reject)=>{
            this.fileModule.updateOne({
                _id: this.ObjectId(_id)
            }, {
                title
            }, (err, res)=>{
                if (err) {
                    console.log(err);
                    reject(err);
                }
                resolve(res);
            });
        });
    }

    search_by_folder() {

    }

    get_file(_id) {
        return new Promise((resolve, reject) => {
            _id = this.ObjectId(_id);
            this.fileModule.findOne({_id}, function (err, docs) {
                if (err){
                    console.log(err);
                    reject(err);
                }
                resolve(docs);
            });
        });
    }

    /**
     * 添加文件
     */
    add_file(user_id, user_name, title, content){
        return new Promise((resolve, reject) => {
            this.fileModule.create({
                _id: this.ObjectId(),
                title,
                content,
                user_id,
                user_name,
                create_time: new Date(),
            }, (err, file)=>{
                if (err) {
                    reject(err);
                }
                resolve(file);
            })
        });
    }

    /**
     * 删除文件
     */
    delete_file(_id){
        return new Promise((resolve, reject) => {
            this.fileModule.deleteOne({
                _id: this.ObjectId(_id)
            }, (err)=>{
                if (err) {
                    reject(err);
                }
                resolve();
            })
        });
    }

    /**
     * 批量删除文件, [_id1, _id2]
     * @param ids
     */
    delete_patch_files(ids) {
        ids = ids.map(value=>{return this.ObjectId(value)});
        console.log(ids);
        return new Promise((resolve, reject) => {
            this.fileModule.deleteMany({
                _id: {$in: ids}
            }, (err)=>{
                if (err) {
                    reject(err);
                }
                resolve();
            })
        });
    }

    copy_file(_id) {
        return new Promise((resolve, reject) => {
            this.get_file(_id)
                .then(result=>{
                    this.add_file(result.user_id, result.user_name, result.title, result.content)
                        .then(f => {
                            resolve(f);
                        }, error => {
                            reject(error);
                        })
                })
        });
    }
}

module.exports = new fileServer();
