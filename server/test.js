function judge(input) {
    let re = new RegExp("^[a-zA-Z]+[0-9]{3}$");
    let res = input.match(re);
    console.log(res);
    if(res === null) {
        return false;
    } else {
        return true;
    }
}

const userServer = require('./userServer');

userServer.validate_user('Andy Yang', '201592009')
    .then(result => {
        console.log(result)
    }, error => {
        console.log(error)
    });
