let serve = require('./server');
let {MONGO_DB} = require('../config');

class userServer extends serve{
    constructor(props) {
        super(props);
        this.userModel = this.mongoose.model('user', this.userSchema, 'user');
    }

    /**
     *  查询某一个用户的所有文件
     *  input: _id
     *  output: files
     */
    files(_id) {
        return new Promise((resolve, reject) => {
            this.get_user(_id)
                .then(value => {
                resolve(value['files']);
            }).then(value => {
                reject(value);
            })
        });
    }

    /**
     *  根据_id查用户
     *  input: _id
     *  output: user // JSON
     */
    get_user(_id) {
        return new Promise((resolve, reject)=>{
            _id = this.ObjectId(_id);
            this.userModel.findOne({
                _id
            }, (err, user)=>{
                if (err) {
                    console.log(err);
                    reject(err);
                }
                resolve(user);
            });
        });
    }

    update_user_files(user_id, files){
        return new Promise((resolve, reject)=>{
            this.userModel.updateOne({
                _id: this.ObjectId(user_id)
            }, {
                files
            }, (err, res) =>{
                if (err) {
                    console.log(res);
                    reject(err);
                }
                resolve(res);
            });

        });
    }

    validate_user(name, password) {
        return new Promise((resolve, reject) => {
            this.userModel.findOne({
                name,
                password
            }, (err, res)=>{
                if (err) {
                    reject(err);
                }
                resolve(res);
            })
        })
    }
}

module.exports= new userServer();
