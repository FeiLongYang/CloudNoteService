// let server = require('./server');
//
// class folderServer extends server{
//     constructor(props) {
//         super(props);
//         this.folderModule = this.mongoose.model('folder', this.folderSchema, 'folder');
//     }
//
//     /**
//      * 创建folder
//      */
//     create_folder(title, user_id, user_name){
//         return new Promise((resolve, reject) => {
//             this.folderModule.create({
//                 title,
//                 user_id,
//                 user_name,
//                 create_time: new Date(),
//                 files: [[], []]
//             }, (err, folder)=>{
//                 if (err) {
//                     reject(err);
//                 }
//                 resolve(folder);
//             });
//         });
//     }
//
//     /**
//      * 更新files
//      */
//     update_files(_id, files) {
//         return new Promise((resolve, reject) => {
//             this.folderModule.updateOne({
//                 _id,
//             }, {
//                 files
//             }, (error, result)=>{
//                 if (error){
//                     reject(error);
//                 }
//                 resolve(result);
//             });
//         });
//     }
//
//     /**
//      * 获取files
//      */
//     get_files(_id) {
//         return new Promise((resolve, reject) => {
//             this.folderModule.findOne({
//                 _id
//             },'files', (error, folder)=>{
//                 if (error) {
//                     reject(error);
//                 }
//                 resolve(folder.files);
//             });
//         });
//     }
// }
//
// module.exports = new folderServer();
