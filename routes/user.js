var express = require('express');
var router = express.Router();
let userServer = require('../server/userServer');
let fileServer = require('../server/fileServer');

/**
 *  查询某一个用户的所有文件
 *  req: {
 *      _id
 *  }
 *
 *  res: {
 *      success: true | false,
 *      error: "",
 *      files: {}
 *  }
 */
router.all('/files', function(req, res, next) {
  userServer.files();
  res.send('respond with a resource');
});

/**
 *  根据_id查用户
 *  req: {
 *      _id
 *  }
 *
 *  res: {
 *      success: true | false,
 *      error: {},
 *      user: {}
 *  }
 */
router.all('/get_user', function(req, res, next) {
  let _id = req.body._id;
  if (_id === undefined){
    return res.send(userServer.result(false, userServer.inputError('_id is undefined'), {}));
  }
  userServer.get_user(_id).then(user=>{
    if (user === null) {
      return res.send(userServer.result(false, userServer.databaseError('_id has no user'), user));
    }
    return res.send(userServer.result(true, {}, user));
  });
});


/**
 * 创建folder
 */
router.post('/create_folder', function (req, res, next) {
  let title = req.body.title;
  let user_id = req.body.user_id;
  let user_name = req.body.user_name;
  let p_folder_id = req.body.p_folder_id;
  let files = JSON.parse(req.body.files);
  function deepParse(file) {
    if (file.type === 'dir') {
      if (file._id === p_folder_id) {
        file.files.push({
          "type": "dir",
          title,
          "_id": userServer.ObjectId(),
          'files': []
        });
      } else {
        file.files.map(f => deepParse(f));
      }
    }
  }
  deepParse(files);
  userServer.update_user_files(user_id, files)
      .then(result=>{
        res.send(userServer.result(true, {}, files));
      }, error=>{
        console.log(error);
        res.send(userServer.result(false, error, {}));
      });
});

/**
 *
 *
 * 删除文件夹
 */
router.post('/delete_folder', function (req, res, next) {
  let user_id = req.body.user_id;
  let folder_id = req.body.folder_id;
  let files = JSON.parse(req.body.files);
  let deleteFiles = [];
  function deepParseGetFiles(file) {
    if (file.type === 'dir') {
      file.files.map(value => {deepParseGetFiles(value)});
    } else if (file.type === 'file'){
      deleteFiles.push(file._id);
    }
  }
  function deepParse(file) {
    if (file.type === 'dir') {
      if (file._id === folder_id) {
        deepParseGetFiles(file);
      } else {
        file.files.map(value => {deepParse(value)});
      }
    }
  }
  deepParse(files);
  function deepParseFilter(file){
    if (file.type === 'dir') {
      file.files = file.files.filter(value=>{return value._id !== folder_id});
      file.files.map(value=>{deepParseFilter(value)});
    }
  }
  deepParseFilter(files);

  Promise.all([userServer.update_user_files(user_id, files), fileServer.delete_patch_files(deleteFiles)])
      .then(result=>{
        res.send(userServer.result(true, {}, files));
      }, error => {
        res.send(userServer.result(false, error, {}));
      });
});

router.post('/update_files', function (req, res, next) {
  let _id = req.body._id;
  let files = JSON.parse(req.body.files);
  userServer.update_user_files(_id, files)
      .then(result=>{
        res.send(userServer.result(true, {}, files));
      }, error=>{
        res.send(userServer.result(false, error, {}));
      })
});

router.post('/validate_user', function (req, res, next) {
  let name = req.body.name;
  let password = req.body.password;
  userServer.validate_user(name, password)
      .then(result=>{
        res.send(userServer.result(true, {}, result));
      }, error =>{
        console.log(error);
        res.send(userServer.result(false, error, {}));
      })
});
module.exports = router;
