var express = require('express');
var router = express.Router();
let fileServer = require('../server/fileServer');
let userServer = require('../server/userServer');
// let folderServer = require('../server/folderServer');
/**
 * 更新文件标题
 */
router.post('/update_file_title', function(req, res, next) {
    let user_id = req.body.user_id;
    let file_id = req.body.file_id;
    let title = req.body.title;
    let files = JSON.parse(req.body.files);
    function deepParse(file) {
        if (file.type === 'dir') {
            file.files.map(f => deepParse(f));
        } else if (file.type === 'file' && file._id === file_id) {
            file['title'] = title;
        }
    }
    deepParse(files);
    Promise.all([fileServer.update_file_title(file_id, title), userServer.update_user_files(user_id, files)])
        .then(result=>{
            return res.send(fileServer.result(true, {}, files));
        }, err => {
            console.log(err);
            return res.send(fileServer.result(false, err, {}));
        });
});

/**
 * 更新文件内容
 */
router.all('/update_file_content', function(req, res, next) {
    let _id = req.body._id;
    let content = req.body.content;
    fileServer.update_file_content(_id, content)
        .then(result => {
            return res.send(fileServer.result(true, {}, result));
            }, err=>{
            return res.send(false, err, {});
        });
});

/**
 * 根据文件夹得到文件夹下的所有文件
 */
router.all('/search_by_folder', function(req, res, next) {
    fileServer.search_by_folder();
    return res.send('respond with a resource');
});

/**
 * user_id
 * file_id
 * files
 * 删除文件
 */
router.post('/delete_file', function (req, res, next) {
    let user_id = req.body.user_id;
    let file_id = req.body.file_id;
    let files = JSON.parse(req.body.files);
    function deepParse(file){
        if (file.type === 'dir'){
            file.files = file.files.filter(f=>{return f._id !== file_id});
            file.files.map(f=>{deepParse(f)});
        }
    }
    deepParse(files);
    Promise.all([userServer.update_user_files(user_id, files), fileServer.delete_file(file_id)])
        .then(result => {
            return res.send(fileServer.result(true, {}, files));
        }, error => {
            return res.send(fileServer.result(false, error, {}));
        });
});


/**
 * 获得文件
 */
router.all('/get_file', function (req, res, next) {
    let _id = req.body._id;
    if (_id === undefined){
        return res.send(fileServer.result(false, '_id is undefined', {}));
    }
    fileServer.get_file(_id)
        .then(result => {
            return res.send(fileServer.result(true, {}, result));
            }, err => {
            console.log(err);
            return res.send(fileServer.result(false, err ,{}));
        });
});

/**
 * 添加文件
 */
router.post('/add_file', function (req, res, next) {
    let user_id = req.param('user_id');
    let user_name = req.param('user_name');
    let p_folder_id = req.param('p_folder_id');
    let title = req.param('title');
    let content = req.param('content');
    let user_files = JSON.parse(req.param('user_files'));
    fileServer.add_file(user_id, user_name, title, content)
        .then(result=>{
            let file_id = result._id.toString();
            function deepParse(file) {
                if (file.type === 'dir'){
                    if (file._id === p_folder_id) {
                        file.files.push({
                            "type": "file",
                            "title": title,
                            "_id": file_id,
                        });
                    } else {
                        file.files.map(value=>{deepParse(value)})
                    }
                }
            }
            deepParse(user_files);
            userServer.update_user_files(user_id, user_files)
                .then(result=>{
                    return res.send(fileServer.result(true, {}, user_files));
                }, error=>{
                    console.log(error);
                    return res.send(fileServer.result(false, error, {}));
                });
            // Promise.all([userServer.update_user_files(user_id, user_files), folderServer.get_files(p_folder_id)])
            //     .then(folder_files=>{
            //         folder_files[1].push(file_id);
            //         folderServer.update_files(p_folder_id, folder_files)
            //             .then(result=>{
            //                 return res.send(fileServer.result(true, {}, user_files));
            //             }, error=>{
            //                 console.log(error);
            //                 return res.send(fileServer.result(false, error, {}));
            //             })
            //     }, error=>{
            //         console.log(error);
            //         return res.send(fileServer.result(false, error, {}));
            //     })
        }, error => {
            console.log(error);
            return res.send(fileServer.result(false, error, {}));
        });
});


/**
 * 复制一份文件夹
 * files
 * user_id
 * user_name
 *
 * @type {Router}
 */
router.post('/copy_dir', function (req, res, next) {
    let files = JSON.parse(req.body.files);
    // let user_id = req.body.user_id;
    // let user_name = req.body.user_name;

    console.log(files);

    let allFileIds = [];

    let allCopyFileProcesses = [];

    function getAllFiles(file) {
        if (file.type === 'file') {
            allFileIds.push(file._id);
            allCopyFileProcesses.push(fileServer.copy_file(file._id));
        }
        if (file.type === 'dir') {
            file.files.map(value=>{getAllFiles(value)});
        }
    }
    getAllFiles(files);
    Promise.all(allCopyFileProcesses)
        .then(result => {
            let change_dict = {};
            for (let i = 0; i < allFileIds.length; i++) {
                change_dict[allFileIds[i]] = result[i]._id.toString();
            }

            function deepParse(file) {
                if (file.type === 'dir') {
                    file._id = fileServer.ObjectId();
                    file.files.map(value => {deepParse(value)});
                }
                if (file.type === 'file') {
                    file._id = change_dict[file._id];
                }
            }
            deepParse(files);
            res.send(fileServer.result(true, {}, files));
        }, error=>{
            res.send(fileServer.result(false, error, {}))
        });
});


module.exports = router;
