let express = require('express');
let router = express.Router();
let multer = require('multer');

let storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'public/images');
    },
    filename: function (req, file, cb) {
        cb(null, file.fieldname + '-' + Date.now())
    }
});
let upload = multer({
    storage: storage,
    dest: 'public/image'
}).single('image');
let userServer = require('../server/userServer');

router.post('/image', upload, function (req, res, next) {
    if (req.file !== null && req.file.filename !== null) {
        res.send(userServer.result(true, {}, req.file.filename));
    }else {
        res.send(userServer.result(false, req, {}));
    }
});

module.exports = router;
